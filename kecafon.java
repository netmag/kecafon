/*
 *  Kecafon, X-efect for web
 *  Copyright (C) 1998 Radim Kolar <hsn@cybermail.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA or
 *  download copy from http://www.gnu.org/copyleft/gpl.html
 *
 */


/* Kecafon 1.1

 (c) Radim Kolar [hsn@cybermail.net]
 
 http://ncic.netmag.cz/apps/nase/kecafon.html
 
 History:
 
 6. 2.1998  v1.0  
 19.2.1998  v1.01
  - added redirect on click to own homepage
  - parameters are reset to default values when loop ends
 22.7.1998 v1.1
   new Spash efects
 
*/

import java.applet.Applet;
import java.awt.*;
import java.io.*;
import java.net.*;
import java.util.*;

public class kecafon extends java.applet.Applet implements Runnable
{

  public static final String COPY="KECAFON 1.1 (C) Radim Kolar (hsn@cybermail.net) 1998.";
  
  private Color bgcolor; 
  private Color textcolor;
  private Color flash;
  
  private Image    image,prev,next;
  private Graphics draw;
  
  private int h,w;
  
  private Vector kecy;
  
  private Thread runner;
  
  private Font textfont;
  
  private int efekt;
  
  private int hstep;
  private int hdelay;

  private int fade,fadedelay;
  private int typeddelay,vdelay,maindelay;
  private int splash;  
  
  private int border,fontsize;
  private String fontname="";
  
  private int sx,sy;
  
  private int charpos[];
  
  public String getAppletInfo()
  {
   return COPY;
  }    

public boolean mouseEnter(Event e, int x, int y){

	showStatus(COPY);

	return(true);

}//end mouseEnter
    
  public void init()
  {
   w=size().width;
   h=size().height;

   kecy=new Vector();

  /* nacteme kecy */
  int i=1;
  String s;
  while( (s=getParameter("text"+(i++)))!=null)
    kecy.addElement(s);
  if(kecy.size()==0) kecy.addElement("NCIC - Nejlepsi Javovska sajta v zemi!");
  
    /* inicializace offscreen bufferu */
   image=createImage(w, h);
   prev=createImage(w, h);
   
   draw=prev.getGraphics();
   draw.setColor(bgcolor);
   draw.fillRect(0, 0, w, h);
   
   draw=image.getGraphics();

  }     

private void initparams()
{  
   /* a jedeme na parametry */
   String s;
   
   s  = getParameter( "bgcolor");

   if( s != null )
            parseparam("bgcolor="+s);
            else
            bgcolor=Color.blue;

   s  = getParameter( "textcolor");

   if( s != null )
            parseparam("textcolor="+s);
            else
            textcolor=Color.yellow;

   s  = getParameter( "flash");

   if( s != null )
            parseparam("flash="+s);
            else
            flash=Color.white;

   s  = getParameter( "hstep");

   if( s != null )
            parseparam("hstep="+s);
            else
            hstep=5;

   s  = getParameter( "hdelay");

   if( s != null )
            parseparam("hdelay="+s);
            else
            hdelay=20;

  s = getParameter( "fade");
   if( s != null )
            parseparam("fade="+s);
            else
             fade=30;                                                

  s = getParameter( "splash");
   if( s != null )
            parseparam("splash="+s);
            else
             splash=30;                                                
             
  s = getParameter( "fadedelay");
   if( s != null )
            parseparam("fadedelay="+s);
            else
             fadedelay=30;                                                

  s = getParameter( "typeddelay");
   if( s != null )
            parseparam("typeddelay="+s);
            else
             typeddelay=30;                                                

  s = getParameter( "vdelay");
   if( s != null )
            parseparam("vdelay="+s);
            else
             vdelay=30;                                                

   s = getParameter( "maindelay");
   if( s != null )
            parseparam("maindelay="+s);
            else
             maindelay=5000;                                                

   s = getParameter( "e");
   if( s != null )
            parseparam("e="+s);
            else
             efekt=2;                                                

   s = getParameter( "border");
   if( s != null )
            parseparam("border="+s);
            else
             border=10;                                                

   s = getParameter( "fontname");
   if( s != null )
            parseparam("fontname="+s);
            else
             fontname="Times";                                                

   s = getParameter( "fontsize");
   if( s != null )
            parseparam("fontsize="+s);
            else
             fontsize=20;                                                
                                                                                                        
  textfont=new Font(fontname,Font.PLAIN,fontsize);
}  
  
  /* param = 'zero=222' */
  private void parseparam(String what)
  {
   int i=what.indexOf("=");
   if(i==-1) return;
   String name=what.substring(0,i);
   String value=what.substring(i+1,what.length());
   if(name.equals("bgcolor")) bgcolor=parseColorString(value);   
   if(name.equals("textcolor")) textcolor=parseColorString(value);
   if(name.equals("flash")) flash=parseColorString(value);
   if(name.equals("hstep")) hstep=Integer.parseInt(value);
   if(name.equals("hdelay"))  hdelay=Integer.parseInt(value);
   if(name.equals("fade"))  fade=Integer.parseInt(value);
   if(name.equals("splash"))  splash=Integer.parseInt(value);  
   if(name.equals("fadedelay"))  fadedelay=Integer.parseInt(value);      
   if(name.equals("typeddelay"))  typeddelay=Integer.parseInt(value);         
   if(name.equals("vdelay"))  vdelay=Integer.parseInt(value);            
   if(name.equals("maindelay"))  maindelay=Integer.parseInt(value);               
   if(name.equals("e"))  efekt=Integer.parseInt(value);                  
   if(name.equals("border"))  border=Integer.parseInt(value);                     
   if(name.equals("fontsize"))  fontsize=Integer.parseInt(value);                        
   if(name.equals("fontname"))  fontname=value;                        
  }
  
  private void calibratefont()
  {
  Font f;
  FontMetrics fm;
  int size=2;
  while(true)
  {
  f=new Font(fontname,Font.PLAIN,size++);
  fm=getFontMetrics(f);
  if(h-2*border<(fm.getAscent()+fm.getDescent())) { f=new Font(fontname,Font.PLAIN,size-2);
                                                   textfont=f;
                                                   return;
                                                 }
  }
    
}
  
  
    public void start()
    {
        // user visits the page, create a new thread

        if (runner == null)
        {
            runner = new Thread(this);
            runner.start();
        }
    }

    public void stop()
    {
        // user leaves the page, stop the thread

        if (runner != null && runner.isAlive())
            runner.stop();

        runner = null;
    }
    
  public void run()
  {
   String text;
   int index=0;
   int counter,rc;
   // calibratefont();
   initparams();
      
   FontMetrics fm=getFontMetrics(textfont);
   sy=(h-fm.getAscent()-fm.getDescent())/2+fm.getAscent();
 
   while(runner!=null)
   {
    if(index==kecy.size()) { index=0; initparams();}
    text=(String)kecy.elementAt(index++);
    
    if(text.charAt(0)=='~') { String s=text.substring(1,text.length());
                              parseparam(s);
                              if(text.indexOf("font")>0)
                                   {
                                    /* menime fontik */
                                    textfont=new Font(fontname,Font.PLAIN,fontsize);
                                    
                                    /* spocitej znovu sy */
                                    fm=getFontMetrics(textfont);
                                    sy=(h-fm.getAscent()-fm.getDescent())/2+fm.getAscent();
                                   }
                              continue;}
    /* SX= prvni znak napisu */
    sx=(w-fm.stringWidth(text))/2;
    /* inicializace pole pozic */
    charpos=new int[text.length()];
    charpos[0]=sx;
    for(int i=1;i<charpos.length;i++)
     charpos[i]=sx+fm.stringWidth(text.substring(0,i));
    
    image=createImage(w, h);
    draw=image.getGraphics();
    
    counter=0;
    while((rc=efektwrap(counter++,text,prev))>0)
    
     { repaint();
       pauzams(rc);}
       
    repaint();
    pauzams(maindelay);
    prev=image;
    
   }
  
}

private int efektwrap(int counter, String text, Image oldimage)
{
 switch(efekt)
 {
  case 1:return efekt1(counter,text,oldimage);
  case 2:
  default:
        return efekt2(counter,text,oldimage);
  case 3:return efekt3(counter,text,oldimage);
  case 4:return efekt4(counter,text,oldimage);
  case 5:return efekt5(counter,text,oldimage);
  case 6:return efekt6(counter,text,oldimage);
  case 7:return efekt7(counter,text,oldimage);  
  case 8:return efekt8(counter,text,oldimage);    
  case 9:return efekt9(counter,text,oldimage);    
  case 10:return efekt10(counter,text,oldimage);    
  case 11:return efekt11(counter,text,oldimage);    
  
 }

}


/* ======== EFEKTY! =========== */

/* appear */
private int efekt1(int counter, String text, Image oldimage)
{
   draw.setColor(bgcolor);
   draw.fillRect(0, 0, w, h);
   draw.setColor(textcolor);
   draw.setFont(textfont);
   draw.drawString(text,sx,sy);
 return 0;
}

/* typped - text */
private int efekt2(int counter, String text, Image oldimage)
{
   draw.setColor(bgcolor);
   draw.fillRect(0, 0, w, h);
   draw.setColor(textcolor);
   draw.setFont(textfont);
   draw.drawString(text.substring(0,counter),sx,sy);
   if(counter==text.length()) return 0; else
 return typeddelay;
}

/* odjezd vlevo */
private int efekt3(int counter, String text, Image oldimage)
{

   draw.setFont(textfont);
   /* nakreslit stary zbytek */
   draw.setColor(bgcolor);
   draw.drawImage(oldimage,-counter*hstep,0,this);
   draw.fillRect(w-counter*hstep,0,w,h);
   draw.setColor(textcolor);
   draw.drawString(text,w-counter*hstep,sy);
   if(w-counter*hstep<=sx) return 0; else
 return hdelay;
}

/* stridani zezdola */
private int efekt4(int counter, String text, Image oldimage)
{

   draw.setFont(textfont);
   /* nakreslit stary zbytek */
   draw.setColor(bgcolor);
   draw.drawImage(oldimage,0,counter,this);
   draw.fillRect(0,h-counter,w,h);
   draw.setColor(textcolor);
   draw.drawString(text,sx,h-counter);
   if(h-counter<=sy) return 0; else
 return vdelay;
}

/* scroll up */
private int efekt5(int counter, String text, Image oldimage)
{
   FontMetrics fm=getFontMetrics(textfont);

   draw.setFont(textfont);
   /* nakreslit stary zbytek */
   draw.setColor(bgcolor);
   draw.drawImage(oldimage,0,-counter,this);
   draw.fillRect(0,h-counter,w,h);
   draw.setColor(textcolor);
   draw.drawString(text,sx,h-counter+fm.getAscent());
   if(h-counter+fm.getAscent()<=sy) return 0; else
 return vdelay;
}

/* zjasneni */
private int efekt6(int counter, String text, Image oldimage)
{
   draw.setColor(bgcolor);
   draw.fillRect(0, 0, w, h);
   float rstep,bstep,gstep;
   rstep=textcolor.getRed()-bgcolor.getRed();
   bstep=textcolor.getBlue()-bgcolor.getBlue();
   gstep=textcolor.getGreen()-bgcolor.getGreen();
   
   rstep/=fade;
   bstep/=fade;
   gstep/=fade;
   Color c=new Color(bgcolor.getRed()+(int)(rstep*counter),
                     bgcolor.getGreen()+(int)(gstep*counter),
                     bgcolor.getBlue()+(int)(bstep*counter));
   draw.setColor(c);
   draw.setFont(textfont);
   draw.drawString(text,sx,sy);
   if(counter<fade) return fadedelay; else
 return 0;

}

/* typped s carkou na konci */
private int efekt7(int counter, String text, Image oldimage)
{
   draw.setColor(bgcolor);
   draw.fillRect(0, 0, w, h);
   draw.setColor(textcolor);
   draw.setFont(textfont);
   if(counter==text.length()) {   draw.drawString(text.substring(0,counter),sx,sy);return 0;} else
   {   draw.drawString(text.substring(0,counter)+"_",sx,sy); return typeddelay;}
}


/* ztmaveni */
private int efekt8(int counter, String text, Image oldimage)
{
   draw.setColor(bgcolor);
   draw.fillRect(0, 0, w, h);
   float rstep,bstep,gstep;
   rstep=bgcolor.getRed()-textcolor.getRed();
   bstep=bgcolor.getBlue()-textcolor.getBlue();
   gstep=bgcolor.getGreen()-textcolor.getGreen();
   
   rstep/=fade;
   bstep/=fade;
   gstep/=fade;
   Color c=new Color(bgcolor.getRed()+(int)(rstep*counter),
                     bgcolor.getGreen()+(int)(gstep*counter),
                     bgcolor.getBlue()+(int)(bstep*counter));
   draw.setColor(c);
   draw.setFont(textfont);
   draw.drawString(text,sx,sy);
   if(counter<fade) return fadedelay+fade-counter; else
 return 0;

}
/* splash nastup z leva */
private int efekt9(int counter, String text, Image oldimage)
{
  draw.setColor(bgcolor);
  draw.fillRect(0, 0, w, h);
  draw.setFont(textfont);
  draw.setColor(textcolor);

 for(int i=0;i<text.length();i++)
 {
//if(counter<splash)
   draw.drawString(text.substring(i,i+1),(int)((float)charpos[i]/(float)splash*(float)counter),sy);
// else
// draw.drawString(text.substring(i,i+1),charpos[i],sy);
 }
 if(counter<splash) return fadedelay; else 
 return 0;
}

/* splash nastup z prava */
private int efekt10(int counter, String text, Image oldimage)
{
  draw.setColor(bgcolor);
  draw.fillRect(0, 0, w, h);
  draw.setFont(textfont);
  draw.setColor(textcolor);

 for(int i=0;i<text.length();i++)
 {
   draw.drawString(text.substring(i,i+1),w-(int)((float)(w-charpos[i])/(float)splash*(float)counter),sy);
 }
 if(counter<splash) return fadedelay; else 
 return 0;
}

/* splash nastup z pro stred */
private int efekt11(int counter, String text, Image oldimage)
{
  draw.setColor(bgcolor);
  draw.fillRect(0, 0, w, h);
  draw.setFont(textfont);
  draw.setColor(textcolor);

 for(int i=0;i<text.length();i++)
 {
   draw.drawString(text.substring(i,i+1),w/2-(int)((float)(w/2-charpos[i])/(float)splash*(float)counter),sy);
 }
 if(counter<splash) return fadedelay; else 
 return 0;
}


/* EFEKTY - KONEC */

private void pauzams(long ms)
{
	 try
			{
				Thread.sleep(ms);
			}
	 catch (InterruptedException e)
	   { }

}

  
  public final void update(Graphics g)
  {
   paint(g);
  }
  
  public final void paint(Graphics g)
  { 
		 g.drawImage(image, 0, 0, this);
  }
  
  private Color parseColorString(String colorString)
    {


        if(colorString.length()==6){
            int R = Integer.valueOf(colorString.substring(0,2),16).intValue();
            int G = Integer.valueOf(colorString.substring(2,4),16).intValue();
            int B = Integer.valueOf(colorString.substring(4,6),16).intValue();
            return new Color(R,G,B);
        }
        else return Color.lightGray;
    }
 
  public boolean mouseDown(Event evt, int x, int y)

  {
        try{
            getAppletContext().showDocument(new URL("http://ncic.netmag.cz/apps/nase/kecafon.html"));
           }
        catch (MalformedURLException e) {};

	return true;

  }
     
}